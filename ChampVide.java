
/**
 * Une classe pour lever une exception levée lorsque le champ lu d'un répertoire est vide 
 * (champs : nom, prénom, numéro de téléphone). 
 *
 * @author (Laurent Pierron)
 * @version (un numéro de version ou une date)
 */
public class ChampVide extends Exception
{
    // nom du champ vide
    private final String nomChamp;

    /**
     * Constructeur d'objets de classe ChampVide
     * 
     * @param nomChamp  le nom du champ vide
     */
    public ChampVide(String nomChamp) {
        super(nomChamp);
        this.nomChamp = nomChamp;
    }

    public String toString() {
        return "Exception champ vide : " + nomChamp;
    }
}
