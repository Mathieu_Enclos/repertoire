import java.io.*;
import java.util.*;

/**
 * Décrivez votre classe Repertoire ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Repertoire
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private final String nomFichier;
    private static final char SEP = ':';
    private static final String[] labels = {"nom      : ", "prénom   : ", "téléphone: "};

    /**
     * Constructeur d'objets de classe Repertoire
     */
    public Repertoire(String nomFichier)
    {
        // initialisation des variables d'instance
        this.nomFichier = nomFichier;
    }

    /**
     * Un exemple de méthode - remplacez ce commentaire par le vôf4tre
     *
     * @return     la somme des deux paramètres
     */
    public void creerFichier()
    throws FichierVide, ChampVide, IOException {
        String entree = "";
        BufferedWriter repertoire = null;
        Scanner scanner = null;
        try {

            scanner = new Scanner(System.in);
            System.out.print("Nombre d'entrées à lire = ");
            int nbEntrees = scanner.nextInt();
            if (nbEntrees <= 0) {
                scanner.close();
                throw new FichierVide(this.nomFichier);
            }

            repertoire = new BufferedWriter(new FileWriter(this.nomFichier));
            for (int i=0; i <nbEntrees; i++) {
                entree = "";
                for (String label : labels) {
                    entree += lireChamp(label, scanner);
                    entree += SEP;
                }
                repertoire.write(entree, 0, entree.length()-1);
                repertoire.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) scanner.close();
            if (repertoire != null) repertoire.close();
        }

    }

    public void afficherRepertoire()
    throws FichierVide, IOException
    {
        String ligne = "";
        boolean fichierVide = true;
        BufferedReader fichier = null;

        try {
            fichier = new BufferedReader(new FileReader(nomFichier));
            while ((ligne = fichier.readLine()) != null) {
                afficheEntree(ligne);
                fichierVide = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fichier != null) fichier.close();
        }

        if (fichierVide) throw new FichierVide(nomFichier);
    }

    public void afficherRepertoireBin(String nomFich)
    throws FichierVide, IOException
    {
        byte car ;
        boolean fichierVide = true;
        boolean finDeFichier = false;
        String ligne = "";
        DataInputStream fichier = null;

        try {
            fichier = new DataInputStream(new FileInputStream(nomFichier));
            while (! finDeFichier) {
                ligne = "";
                try {
                    car = fichier.readByte();
                    while ((car != 10) && (car != 13)){
                        ligne += (char)car;
                        car = fichier.readByte();
                    }
                } catch  (EOFException e) {
                    finDeFichier = true;
                }
                afficheEntree(ligne);
                fichierVide = false;
            }
            fichier.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fichier != null) fichier.close();
        }

        if (fichierVide) throw new FichierVide(nomFichier); 
    }

    private String lireChamp(String champ, Scanner scanner) 
    throws ChampVide
    {
        System.out.print("Saisir " + champ+" = ");
        String entree = scanner.next();
        if (entree.isEmpty()) throw new ChampVide(champ);
        return entree;   
    }

    private void afficheEntree(String ligne)
    {
        String[] valeurs = ligne.split("" + SEP);
        if (valeurs.length == labels.length) {
            for (int i=0; i < labels.length; i++) {
                System.out.println(labels[i] + valeurs[i]);
            }
            System.out.println("=====================================");
        }
    }

    public void creerRepertoireObj(Personne[] tabPers, String nomFich)
    throws IOException
    {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(nomFich));
            for(Personne personne : tabPers) {
                oos.writeObject(personne);
            }
            oos.close();
        } catch (IOException e) {
            System.out.println("erreur d’E/S");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("erreur hors E/S");
            e.printStackTrace();
        } finally {
            if (oos != null) oos.close();
        }
    }
}
